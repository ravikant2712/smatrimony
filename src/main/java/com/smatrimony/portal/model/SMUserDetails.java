package com.smatrimony.portal.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.json.JSONException;
import org.json.JSONObject;

import com.smatrimony.portal.utils.DateParser;


@Entity
@Table(name = "sm_user_details")
public class SMUserDetails implements Serializable{
	
	
	private static final long serialVersionUID = 1L;

	@Id()
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long user_detail_id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false)
	private SMUser user_id;
	@Column
	private Date date_of_birth;
	@Column
	private String gender;
	@Column
	private Integer profile_created_by;
	@Column
	private Integer religion;
	@Column
	private Integer mother_tongue;
	@Column(columnDefinition = "text")
	private String Userdescription;
	@Column(columnDefinition = "text")
	private String profile_photo;
	@Column(columnDefinition = "text")
	private String registration_id;
	@Column
	private String user_source;
	@Column
	private Date dt_last_login;
	@Column
	private Date dt_last_logout;
	@Column
	private Integer flg_deleted = 0;
	@Column
	private Date dt_created;
	@Column
	private Date dt_updated;

	public Long getUser_detail_id() {
		return user_detail_id;
	}
	public void setUser_detail_id(Long user_detail_id) {
		this.user_detail_id = user_detail_id;
	}
	public SMUser getUser_id() {
		return user_id;
	}
	public void setUser_id(SMUser user_id) {
		this.user_id = user_id;
	}
	public Date getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Integer getProfile_created_by() {
		return profile_created_by;
	}
	public void setProfile_created_by(Integer profile_created_by) {
		this.profile_created_by = profile_created_by;
	}
	public Integer getReligion() {
		return religion;
	}
	public void setReligion(Integer religion) {
		this.religion = religion;
	}
	public Integer getMother_tongue() {
		return mother_tongue;
	}
	public void setMother_tongue(Integer mother_tongue) {
		this.mother_tongue = mother_tongue;
	}
	public String getUserdescription() {
		return Userdescription;
	}
	public void setUserdescription(String userdescription) {
		Userdescription = userdescription;
	}
	public String getProfile_photo() {
		return profile_photo;
	}
	public void setProfile_photo(String profile_photo) {
		this.profile_photo = profile_photo;
	}
	public String getRegistration_id() {
		return registration_id;
	}
	public void setRegistration_id(String registration_id) {
		this.registration_id = registration_id;
	}
	public String getUser_source() {
		return user_source;
	}
	public void setUser_source(String user_source) {
		this.user_source = user_source;
	}
	public Date getDt_last_login() {
		return dt_last_login;
	}
	public void setDt_last_login(Date dt_last_login) {
		this.dt_last_login = dt_last_login;
	}
	public Date getDt_last_logout() {
		return dt_last_logout;
	}
	public void setDt_last_logout(Date dt_last_logout) {
		this.dt_last_logout = dt_last_logout;
	}
	public Integer getFlg_deleted() {
		return flg_deleted;
	}
	public void setFlg_deleted(Integer flg_deleted) {
		this.flg_deleted = flg_deleted;
	}
	public Date getDt_created() {
		return dt_created;
	}
	public void setDt_created(Date dt_created) {
		this.dt_created = dt_created;
	}
	public Date getDt_updated() {
		return dt_updated;
	}
	public void setDt_updated(Date dt_updated) {
		this.dt_updated = dt_updated;
	}
	public JSONObject toJson() {
		
		JSONObject returnJson = new JSONObject();
		try
		{
			returnJson.put("user_detail_id", user_detail_id);
			returnJson.put("user_id", user_id.getUser_id());
			returnJson.put("date_of_birth", new SimpleDateFormat("dd/MM/yyyy").format(date_of_birth));
			returnJson.put("gender", gender);
			returnJson.put("profile_created_by", profile_created_by);
			returnJson.put("religion", religion);
			returnJson.put("mother_tongue", mother_tongue);
			returnJson.put("profile_photo", profile_photo);
			returnJson.put("registration_id", registration_id);
			returnJson.put("user_source", user_source);
			returnJson.put("dt_last_login", dt_last_login);
			returnJson.put("dt_last_logout", dt_last_logout);
			returnJson.put("flg_deleted", flg_deleted);
			returnJson.put("dt_created", dt_created);
			returnJson.put("dt_updated", dt_updated);
		}catch (JSONException e) {}
		return returnJson;
	}

	public SMUserDetails fromJson(JSONObject jsonObject) {
        try
        {
            if (jsonObject.has("date_of_birth")) {
            	String date = jsonObject.getString("date_of_birth");
				if(!"".equalsIgnoreCase(date))
	            {
	                this.setDate_of_birth(DateParser.convertToDate(jsonObject.getString("date_of_birth")));
	            }else
	            {
	            	String tempDate = "01/01/1980";
	                this.setDate_of_birth(DateParser.convertToDateSlash(tempDate));
	            }
            }
            if (jsonObject.has("gender")) {
                this.setGender(jsonObject.getString("gender"));
            }
            if (jsonObject.has("profile_created_by")) {
                this.setProfile_created_by(jsonObject.getInt("profile_created_by"));
            }
            if (jsonObject.has("religion")) {
                this.setReligion(jsonObject.getInt("religion"));
            }
            if (jsonObject.has("mother_tongue")) {
                this.setMother_tongue(jsonObject.getInt("mother_tongue"));
            }
            if (jsonObject.has("profile_photo")) {
                this.setProfile_photo(jsonObject.getString("profile_photo"));
            }
            if (jsonObject.has("registration_id")) {
                this.setRegistration_id(jsonObject.getString("registration_id"));
            }
            if (jsonObject.has("dt_last_login")) {
                this.setDt_last_login(new Date());
            }
            if (jsonObject.has("dt_last_logout")) {
                this.setDt_last_login(new Date());
            }
            if (jsonObject.has("user_source")) {
                this.setUser_source(jsonObject.getString("user_source"));
            }  
        }catch (JSONException e) {}
        return this;
    }	
	

}

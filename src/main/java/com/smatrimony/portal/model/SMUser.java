package com.smatrimony.portal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONException;
import org.json.JSONObject;

@Entity
@Table(name = "sm_user")
public class SMUser {
	
	@Id()
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long user_id;
	@Column
	private String firstname;
	@Column
	private String lastname;
	@Column
	private String email;
	@Column
	private Long mobile;
	@Column
	private String password;
	@Column
	private Boolean is_email_varified = false;
	@Column
	private Boolean is_mobile_varified = false;	
	@Column
	private Boolean is_active = false;
	@Column
	private Integer flg_deleted = 0;
	@Column
	private Date dt_created;
	@Column
	private Date dt_updated;
	@Column
	private Integer status = 0;
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getIs_email_varified() {
		return is_email_varified;
	}
	public void setIs_email_varified(Boolean is_email_varified) {
		this.is_email_varified = is_email_varified;
	}
	public Boolean getIs_mobile_varified() {
		return is_mobile_varified;
	}
	public void setIs_mobile_varified(Boolean is_mobile_varified) {
		this.is_mobile_varified = is_mobile_varified;
	}
	public Boolean getIs_active() {
		return is_active;
	}
	public void setIs_active(Boolean is_active) {
		this.is_active = is_active;
	}
	public Integer getFlg_deleted() {
		return flg_deleted;
	}
	public void setFlg_deleted(Integer flg_deleted) {
		this.flg_deleted = flg_deleted;
	}
	public Date getDt_created() {
		return dt_created;
	}
	public void setDt_created(Date dt_created) {
		this.dt_created = dt_created;
	}
	public Date getDt_updated() {
		return dt_updated;
	}
	public void setDt_updated(Date dt_updated) {
		this.dt_updated = dt_updated;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public JSONObject toJson() {
			
			JSONObject returnJson = new JSONObject();
			try
			{
				returnJson.put("user_id", user_id);
				returnJson.put("firstname", firstname);
				returnJson.put("lastname", lastname);
				returnJson.put("email", email);
				returnJson.put("mobile", mobile);
				returnJson.put("is_email_varified", is_email_varified);
				returnJson.put("is_mobile_varified", is_mobile_varified);
				returnJson.put("is_active", is_active);
				returnJson.put("flg_deleted", flg_deleted);
				returnJson.put("dt_created", dt_created);
				returnJson.put("dt_updated", dt_updated);
				returnJson.put("status", status);
			}catch (JSONException e) {}
			return returnJson;
		}

    public SMUser fromJson(JSONObject jsonObject) {
        try
        {
            if (jsonObject.has("firstname")) {
                this.setFirstname(jsonObject.getString("firstname"));
            }
            if (jsonObject.has("lastname")) {
                this.setLastname(jsonObject.getString("lastname"));
            }
            if (jsonObject.has("email")) {
                this.setEmail(jsonObject.getString("email"));
            }
            if (jsonObject.has("mobile")) {
                this.setMobile(Long.valueOf(jsonObject.getString("mobile")));
            }
            if (jsonObject.has("password")) {
                this.setPassword(jsonObject.getString("password"));
            }
        }catch (JSONException e) {}
        return this;
    }
	
	
}

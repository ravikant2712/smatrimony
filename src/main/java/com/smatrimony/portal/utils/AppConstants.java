package com.smatrimony.portal.utils;

public class AppConstants {

	public static final String prefixClassLevel = "/api";
	public static final String prefixMethodLevel = "/v1.0";

	public static final String MATRIMONY_WEB = "/matrimony_web";
}

package com.smatrimony.portal.utils;

import java.io.File;
import java.util.Date;
import java.util.regex.Pattern;

import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class AppUtils {
	
	public static boolean isValidEmail(String email) 
    { 
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                            "[a-zA-Z0-9_+&*-]+)*@" + 
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                            "A-Z]{2,7}$";               
        Pattern pat = Pattern.compile(emailRegex); 
        if (email == null) 
            return false; 
        return pat.matcher(email).matches(); 
    } 
	
	public static String encriptProductId(String value)
	{
		return BCrypt.hashpw(value, BCrypt.gensalt());
	}
	
	public static String convertDateTOAgo(Date date)
	{
		PrettyTime p = new PrettyTime();
		System.out.println(p.format(date));
		return p.format(date);
	}
	
	
	  public static String getFileExtension(String fileName) {
	        String extension = "";
	        try {
	    		if(fileName.contains(".") && fileName.lastIndexOf(".")!= 0)
	    		{
	    			extension="." + fileName.substring(fileName.lastIndexOf(".")+1);
	    		}
	        } catch (Exception e) {
	            extension = "";
	        }
	 
	        return extension;
	 
	    }

}

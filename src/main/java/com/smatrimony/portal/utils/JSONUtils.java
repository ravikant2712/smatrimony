package com.smatrimony.portal.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONUtils {

    public static JSONObject convertToMSG(boolean isDataFound) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if (isDataFound) {
            jsonObject.put("MSG", "EXIST");
        } else {
            jsonObject.put("MSG", "NOT EXIST");
        }
        return  jsonObject;
    }
}

package com.smatrimony.portal.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {
	
	public static Date convertToDateSlash(String data) {
		if (data != null && !"".equals(data.trim())) {

			try {
				DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
				DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = inputFormat.parse(data);
				String outputDate = outputFormat.format(date);
				Date dt = outputFormat.parse(outputDate);
			//	log.debug("::::::::::::Final Date is + dt");
				return dt;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static Date convertToDate(String data) {
		if (data != null && !"".equals(data.trim())) {

			try {
				DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
				Date date = inputFormat.parse(data);
				return date;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}


}

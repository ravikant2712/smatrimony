package com.smatrimony.portal.service.impl;

import java.util.Date;
import java.util.List;

import com.smatrimony.portal.utils.JSONUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.smatrimony.portal.model.SMUser;
import com.smatrimony.portal.model.SMUserDetails;
import com.smatrimony.portal.service.UserService;

@Component
public class UserServiceImpl implements UserService {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	@Override
	public JSONObject createNewUser(JSONObject jsonObject) {
		Session session = sessionFactory.getCurrentSession();
		JSONObject outputJsonObj = new JSONObject();
		try
		{
			SMUser userMaster = new SMUser().fromJson(jsonObject);

			if (isUserExistsBy(userMaster.getEmail(), "email")) {
				return JSONUtils.convertToMSG(true);
			}
            if (isUserExistsBy(userMaster.getMobile()+"", "mobile")) {
                return JSONUtils.convertToMSG(true);
            }

			userMaster.setFlg_deleted(0);
			userMaster.setDt_created(new Date());
			userMaster.setDt_updated(new Date());
			userMaster.setIs_email_varified(false);
			userMaster.setIs_mobile_varified(false);

			userMaster.setPassword(BCrypt.hashpw(userMaster.getPassword(), BCrypt.gensalt()));
			session.saveOrUpdate(userMaster);
			
			SMUserDetails userDetails = null;
			Criteria cr = session.createCriteria(SMUserDetails.class);
			cr.add(Restrictions.eq("user_id", userMaster));
			userDetails = (SMUserDetails) cr.uniqueResult();

			if (userDetails == null) {
				userDetails = new SMUserDetails().fromJson(jsonObject);
				userDetails.setUser_id(userMaster);
				userDetails.setDt_created(new Date());
				userDetails.setDt_updated(new Date());
				userDetails.setFlg_deleted(0);
			} else {
				userDetails.setDt_updated(new Date());
			}
			session.saveOrUpdate(userDetails);
			outputJsonObj.put("User", userMaster.toJson());
			outputJsonObj.put("UserDetails", userDetails.toJson());
		}catch (JSONException e) {
			e.printStackTrace();
		}
		return outputJsonObj;
	}

    @Override
	public boolean isUserExistsBy(String val, String columnText) {
		JSONObject jsonObject = new JSONObject();
			Session session = sessionFactory.getCurrentSession();
			Criteria cr = session.createCriteria(SMUser.class);
			
			cr.add(Restrictions.eq(columnText, "email".equals(columnText) ? val : Long.parseLong(val)));
			List<SMUser> list = cr.list();

			if (list != null && list.size() > 0) {
				return true;
			} else {
				return false;
			}	
	}

	@Transactional
	@Override
	public SMUser userLogin(JSONObject jsonObject) {
		Session session = sessionFactory.getCurrentSession();
		SMUser users = null;
		try
		{
			String username = jsonObject.getString("username");
			boolean pwd_nReq = false;
			if (jsonObject.has("pwd_nReq")) {
				pwd_nReq = true;
			}

			Criteria cr = session.createCriteria(SMUser.class);
			cr.add(Restrictions.eq("email", username));
			users = (SMUser) cr.uniqueResult();
			

			if (users != null) {
				System.out.println("User found --> " +users.getUser_id());
				if (pwd_nReq) {
					return users;
				} else {
					String password = jsonObject.getString("password");	
					if (checkPwd(password, users.getPassword())) {
						return users;
					} else {
						return null;
					}
				}
			} else {
				return null;
			}			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}
	
	public boolean checkPwd(String plain, String enc) {
		boolean match = false;

		if (BCrypt.checkpw(plain, enc)) {
			match = true;
		}
	//	log.debug("PASSWORD     " + plain + " ENC  " + enc + " mATCH=" + match);
		return match;
	}
}

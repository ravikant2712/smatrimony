package com.smatrimony.portal.service.impl;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.smatrimony.portal.service.ResponseGenerator;

@Scope("prototype")
@Component
public class ResponseGeneratorImpl implements ResponseGenerator{
	
	@Override
	public String generateResponse(String responseString, Integer responsecodevalue, String msg) {

		JSONObject serviceresponse = new JSONObject();
		try
		{
			serviceresponse.put("status", responsecodevalue);
			serviceresponse.put("message", msg);
			if (responseString != null) {
				JSONObject respoObj = new JSONObject(responseString);
				if (respoObj.has("payload")) {
					serviceresponse =new JSONObject(responseString);
				
				} else {
					serviceresponse.put("payload", new JSONObject(responseString));
				}
			}		
		}catch (JSONException e) {
			// TODO: handle exception
		}
		
		return serviceresponse.toString();
	}
	
	@Override
	public String generateResponse(String responseString) {
		JSONObject jsonObject = new JSONObject();
		try
		{
		    JSONObject inputJson = new JSONObject(responseString);
			if (inputJson.has("RESULT")) {
				if (inputJson.getString("RESULT").equalsIgnoreCase("OK")) {
					jsonObject = new JSONObject(generateResponse(responseString, RESPONSE_OK, "SUCCESS"));
				}
				if (inputJson.getString("RESULT").equalsIgnoreCase("KO")) {
					String erro = "ERROR";
					if (inputJson.has("MSG")) {
						erro = inputJson.getString("MSG");
					}
					jsonObject = new JSONObject(generateResponse(responseString, RESPONSE_ERROR, erro));
				}
			} else {
				jsonObject = new JSONObject(generateResponse(null, RESPONSE_ERROR, "UNKNOWN ERROR"));
			}			
		}catch (JSONException e) {	}
		
		return jsonObject.toString();
	}

}

package com.smatrimony.portal.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.AbstractFileResolvingResource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.smatrimony.portal.config.FileDirectoryConfig;
import com.smatrimony.portal.config.FileStorageProperties;
import com.smatrimony.portal.exception.FileStorageException;
import com.smatrimony.portal.exception.MyFileNotFoundException;
import com.smatrimony.portal.utils.AppConstants;
import com.smatrimony.portal.utils.AppUtils;

@Service
public class FileStorageService {
	
	private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(MultipartFile file, int file_type_id, String folderPath) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        
    	String targetImageName = "";
    	if(file_type_id == FileDirectoryConfig.UploadType.IMAGE)
    	{
    		targetImageName = "IMAGE_" + System.currentTimeMillis() +  AppUtils.getFileExtension(fileName);	
    	}else if(file_type_id == FileDirectoryConfig.UploadType.ZIP)
    	{
    		targetImageName = "Package_" + System.currentTimeMillis() +  AppUtils.getFileExtension(fileName);	
    	}else
    	{
    		targetImageName =  System.currentTimeMillis() +  AppUtils.getFileExtension(fileName);	    		
    	}
        try {
        	Path banner= null;
        	Path finalLocation = Paths.get(this.fileStorageLocation.toString(), folderPath);
        	Files.createDirectories(Paths.get(finalLocation.toUri()));
        	File bannerPath = new File(finalLocation.toString());
         	if(!bannerPath.exists())
        	{
        		bannerPath.mkdir();
        	}
        	banner = Paths.get(finalLocation.toString(), fileName);
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
   
            Files.copy(file.getInputStream(), banner, StandardCopyOption.REPLACE_EXISTING);
         /*   String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
					.path(AppConstants.prefixClassLevel)
					.path( AppConstants.prefixMethodLevel)
					.path("/files/")
					.path("/downloadFile/")
					.path(FileDirectoryConfig.BANNER_PATH)
					.path(targetImageName).toUriString();*/
            
            //Add controller with downloadFile post method to get files
            String storePath = FileDirectoryConfig.BANNER_PATH + fileName;
            return storePath;
        } catch (Exception ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName, String path) {
        try 
        {
        	Path filePath = null;
        	if(path != null)
        	{
        		Path temp  = Paths.get(this.fileStorageLocation.toString(), path);
                filePath = temp.resolve(fileName).normalize();
        	}else
        	{
                filePath = this.fileStorageLocation.resolve(fileName).normalize();        		
        	}
        	
            Resource resource =  new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

}

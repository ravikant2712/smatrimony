package com.smatrimony.portal.service;

import com.smatrimony.portal.model.SMUser;
import org.json.JSONObject;

public interface UserService {
	
	public JSONObject createNewUser(JSONObject jsonObject);
	//public JSONObject updateUser(JSONObject jsonObject);
	public boolean isUserExistsBy(String value, String colText);
	public SMUser userLogin(JSONObject jsonObject);
 
}

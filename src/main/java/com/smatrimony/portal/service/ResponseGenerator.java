package com.smatrimony.portal.service;

public interface ResponseGenerator {
	
	public static final Integer RESPONSE_OK = 200;
	public static final Integer RESPONSE_WARNING = 300;
	public static final Integer RESPONSE_ERROR = 500;
	public static final Integer BAD_REQUEST = 400;
	public static final Integer NOT_FOUND = 404;
	//public static final Integer INTERNAL_SERVER_ERROR = 500;
	public static final Integer GATEWAY_TIMEOUT = 504;
	
	String generateResponse(String responseString,Integer responsecodevalue,String msg);

	String generateResponse(String responseString );
	
}

package com.smatrimony.portal.config;

public class FileDirectoryConfig {
	
	public static String BASE_PATH = "/web";
	
	public static String BANNER_PATH = BASE_PATH + "/banner/";
	
	
	
	public static class UploadType{
		public static int IMAGE = 1;
		public static int ZIP = 2;
	}

}

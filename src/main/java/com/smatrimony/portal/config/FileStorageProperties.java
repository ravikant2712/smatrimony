package com.smatrimony.portal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
	
	private String upload_dir;

    public String getUploadDir() {
        return upload_dir;
    }

    public void setUploadDir(String upload_dir) {
        this.upload_dir = upload_dir;
    }

}

package com.smatrimony.portal.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.smatrimony.portal.model.Role;
import com.smatrimony.portal.model.RoleName;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{
	 Optional<Role> findByName(RoleName roleName);
}

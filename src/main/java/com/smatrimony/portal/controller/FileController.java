package com.smatrimony.portal.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.smatrimony.portal.service.ResponseGenerator;
import com.smatrimony.portal.service.impl.FileStorageService;
import com.smatrimony.portal.utils.AppConstants;

@RestController
@RequestMapping(AppConstants.prefixClassLevel + AppConstants.prefixMethodLevel + "/files")
public class FileController {
	
	@Autowired
	ResponseGenerator responseGenerator;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName, null);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                // Set Header to inline mode to view on browser
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                // Set Header to attachment mode to view on browser                
            //  .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

	
	@GetMapping("/downloadFile/{foldername}/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("foldername") String foldername, @PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
		System.out.println("Folder Name " + foldername);
        Resource resource = fileStorageService.loadFileAsResource(fileName, foldername);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                // Set Header to inline mode to view on browser
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                // Set Header to attachment mode to view on browser                
            //  .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	@GetMapping("/downloadFile/{foldername}/{foldername1}/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("foldername") String foldername,
    		@PathVariable("foldername1") String foldername1,
    		@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
		
		System.out.println("Folder Name " + foldername);
		String finalPathFolder = "/" + foldername + "/" + foldername1;
        Resource resource = fileStorageService.loadFileAsResource(fileName, finalPathFolder);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                // Set Header to inline mode to view on browser
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                // Set Header to attachment mode to view on browser                
            //  .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	
	@GetMapping("/downloadFile/{foldername}/{foldername1}/{foldername2}/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("foldername") String foldername,
    		@PathVariable("foldername1") String foldername1,
    		@PathVariable("foldername2") String foldername2,
    		@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
		
		String finalPathFolder = "/" + foldername + "/" + foldername1+ "/" + foldername2;
        Resource resource = fileStorageService.loadFileAsResource(fileName, finalPathFolder);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                // Set Header to inline mode to view on browser
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                // Set Header to attachment mode to view on browser                
            //  .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
	
	@GetMapping("/downloadFile/{foldername}/{foldername1}/{foldername2}/{foldername3}/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("foldername") String foldername,
    		@PathVariable("foldername1") String foldername1,
    		@PathVariable("foldername2") String foldername2,
    		@PathVariable("foldername3") String foldername3,
    		@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
		
		String finalPathFolder = "/" + foldername + "/" + foldername1+ "/" + foldername2+ "/" + foldername3;
        Resource resource = fileStorageService.loadFileAsResource(fileName, finalPathFolder);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                // Set Header to inline mode to view on browser
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + resource.getFilename() + "\"")
                // Set Header to attachment mode to view on browser                
            //  .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }


}

package com.smatrimony.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.smatrimony.portal.model.SMUser;
import com.smatrimony.portal.service.ResponseGenerator;
import com.smatrimony.portal.service.UserService;
import com.smatrimony.portal.utils.AppConstants;

@RestController
@RequestMapping(AppConstants.prefixClassLevel + AppConstants.prefixMethodLevel + AppConstants.MATRIMONY_WEB)
public class SMatrimonyWebController {

	@Autowired
	ResponseGenerator responseGenerator;
	
	@Autowired
	UserService smatrimonyWebCrudService;
	 
	
	@SuppressWarnings("null")
	@ResponseBody
	@RequestMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public String register(@RequestBody String jsonString, HttpServletRequest httpServletRequest) {

		String response = "";
		try {
			JSONObject outputJsonObject = new JSONObject();
			JSONObject jsonObject = new JSONObject(jsonString);
			if (jsonObject.has("firstname") && jsonObject.has("email") && jsonObject.has("password")
					&& jsonObject.has("mobile")) {
				outputJsonObject = smatrimonyWebCrudService.createNewUser(jsonObject);
				if (outputJsonObject != null) {
					if (outputJsonObject.has("MSG"))
						response = responseGenerator.generateResponse(outputJsonObject.toString(),
								ResponseGenerator.RESPONSE_WARNING, "User Already Exist");
					else
						response = responseGenerator.generateResponse(outputJsonObject.toString(),
								ResponseGenerator.RESPONSE_OK, "SUCCESS");
				} else {
					response = responseGenerator.generateResponse(outputJsonObject.toString(),
							ResponseGenerator.RESPONSE_ERROR, "User Not Created");
				}
			} else {
				response = responseGenerator.generateResponse(outputJsonObject.toString(), ResponseGenerator.NOT_FOUND,
						"Please fill all the required fields");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@ResponseBody
	@RequestMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public String login(@RequestBody String json, HttpServletRequest httpServletRequest) {

		String response = "";
		JSONObject outputJsonObject = new JSONObject();
		try {
			JSONObject jsonObject = new JSONObject(json);

			if (jsonObject.has("username") == false || jsonObject.getString("username").isEmpty()
					|| jsonObject.has("password") == false || jsonObject.getString("password").isEmpty()) {

				outputJsonObject.put("RESULT", "KO");
				outputJsonObject.put("MSG", "UserName & Password is must.");
				response = responseGenerator.generateResponse(outputJsonObject.toString());
			} else {
				SMUser users = smatrimonyWebCrudService.userLogin(jsonObject);
				if (users != null) {

					outputJsonObject.put("User", users.toJson());
					outputJsonObject.put("RESULT", "OK");

				} else {
					outputJsonObject.put("RESULT", "KO");
					outputJsonObject.put("MSG", "User is Not Available");
				}
				response = responseGenerator.generateResponse(outputJsonObject.toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	

}
 
package com.smatrimony.portal;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import com.smatrimony.portal.config.FileStorageProperties;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@EntityScan(basePackageClasses = {SMatrimonyApplication.class, Jsr310JpaConverters.class} )
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class SMatrimonyApplication extends SpringBootServletInitializer{
	
	@PostConstruct
	void init()
	{
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	public static void main(String[] args) {
		SpringApplication.run(SMatrimonyApplication.class, args);
	}

}
